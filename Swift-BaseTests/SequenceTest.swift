//
//  SequenceTest.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 01.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import XCTest
import SwiftBase

class SequenceTest: XCTestCase {

    func testBooleanCombine() {
        XCTAssertTrue([1,2,3,4,5].all { $0 > 0 }, "'all' combinator is wrong")
        XCTAssertFalse([1,2,3,-4,5].all { $0 > 0 }, "'all' combinator is wrong")
        XCTAssertTrue([1,2,3,-4,5].any { $0 < 0 }, "'any' combinator is wrong")
        XCTAssertFalse([1,2,3,4,5].any { $0 < 0 }, "'any' combinator is wrong")
    }

    func testReduce1() {
        XCTAssertEqual([1,2,3].reduce1((+)), 6)
    }
}
