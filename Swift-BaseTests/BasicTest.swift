//
//  BasicTest.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 09.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import XCTest
import SwiftBase

class BasicTest: XCTestCase {

    func testFlip() {
        XCTAssertEqual(flip((-))(3, 2), 2 - 3)
    }

    func testConst() {
        XCTAssertEqual(const(42)(10), 42)
    }
}
