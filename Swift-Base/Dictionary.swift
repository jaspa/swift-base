//
//  Dictionary.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 16.01.16.
//  Copyright © 2016 Janek Spaderna. All rights reserved.
//

public extension Dictionary {
    init<S: Sequence>(sequence: S) where S.Iterator.Element == (Key, Value) {
        self = [:]
        for (key, value) in sequence {
            self[key] = value
        }
    }

    func mapKeys<T: Hashable>(_ f: (Key) -> T) -> [T: Value] {
        return Dictionary<T, Value>(sequence: zip(keys.map(f), values))
    }

    func mapValues<T>(_ f: (Value) -> T) -> [Key: T] {
        return Dictionary<Key, T>(sequence: zip(keys, values.map(f)))
    }

    func map<T: Hashable, U>(keys keyF: (Key) -> T, values valueF: (Value) -> U) -> [T: U] {
        return Dictionary<T, U>(sequence: zip(keys.map(keyF), values.map(valueF)))
    }
}
