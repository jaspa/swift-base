//
//  Sequence.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 30.04.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation

public extension Sequence {
    /// Test if *all* elements in the sequence satisfy a given predicate.
    ///
    /// - returns: Returns `true` if the predicate returns `true` for all elements in the sequence, else `false`.
    func all(_ pred: (Iterator.Element) -> Bool) -> Bool {
        return reduce(true) { a, e in a && pred(e) }
    }

    /// Test if at least one element in the sequence satisfies a given predicate.
    ///
    /// - returns: Returns `true` if the predicate returns `true` for at least one element in the sequence, else `false`.
    func any(_ pred: (Iterator.Element) -> Bool) -> Bool {
        return reduce(false) { a, e in a || pred(e) }
    }

    /// Combine the elements using the given function function.
    ///
    /// - parameter combine: The function to combine the elements.
    /// - returns: The last value returned by `combine` or, if there is only one
    ///            element in the sequence, this element is returned.
    /// - precondition: The receiver must not be empty.
    func reduce1(_ combine:
      (_ accumulator: Iterator.Element, _ element: Iterator.Element)
        -> Iterator.Element
    ) -> Iterator.Element {
        var g = makeIterator()
        let ini = g.next()!
        return IteratorSequence(g).reduce(ini, combine)
    }

    func find(_ pred: (Iterator.Element) -> Bool) -> Iterator.Element? {
        for elem in self {
            if pred(elem) {
                return elem
            }
        }

        return nil
    }
}
