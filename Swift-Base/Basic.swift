//
//  Basic.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 03.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation

/// The identity function.
public func identity<T>(_ x: T) -> T {
    return x
}

/// The constant function.
public func const<T, U>(_ x: T) -> (U) -> T {
    return { _ in x }
}

/// Flip the arguments in a function.
public func flip<T, U, V>(_ f: @escaping (T, U) -> V) -> (U, T) -> V {
    return { f($1, $0) }
}

/// Flip the arguments in a curried function.
public func flip<T, U, V>(_ f: @escaping (T) -> (U) -> V) -> (U) -> (T) -> V {
    return { u in { t in f(t)(u) } }
}
