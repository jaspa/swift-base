//
//  NGeneratorOfOne.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 09.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation

/// A generator and sequence which returns a given value *n* times.
public struct NGeneratorOfOne<T> {
    fileprivate let value: T
    fileprivate var count: Int

    public init(count: Int, value: T) {
        self.value = value
        self.count = count - 1
    }
}

extension NGeneratorOfOne: IteratorProtocol {

    /// Decreases the remaining count and returns the given value or `nil` if the count has sunk below zero.
    public mutating func next() -> T? {

        if count < 0 {
            return nil
        }

        count -= 1;
        return value
    }
}

extension NGeneratorOfOne: Sequence {

    /// `NGeneratorOfOne<T>` is also a `SequenceType`, so it `generate`s a copy of itself.
    public func makeIterator() -> NGeneratorOfOne {
        return self
    }
}
