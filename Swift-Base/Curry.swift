//
//  Curry.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 27.04.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation

// MARK: - Currying

/// Curries a binary function `f`, producing a function which can be partially applied.
public func curry<A, B, C>(_ f: @escaping (A, B) -> C) -> (A) -> (B) -> C {
    return { a in
           { b in f(a, b) } }
}

/// Curries a ternary function `f`, producing a function which can be partially applied.
public func curry<A, B, C, D>(_ f: @escaping (A, B, C) -> D) -> (A) -> (B) -> (C) -> D {
    return { a in
           { b in
           { c in f(a, b, c) } } }
}

/// Curries a quaternary function, `f`, producing a function which can be partially applied.
public func curry<A, B, C, D, E>(_ f: @escaping (A, B, C, D) -> E) -> (A) -> (B) -> (C) -> (D) -> E {
    return { a in
           { b in
           { c in
           { d in f (a, b, c, d) } } } }
}


// MARK: - Uncurrying

/// Uncurries a curried binary function `f`, producing a function which can be applied to a tuple.
public func uncurry<A, B, C>(_ f: @escaping (A) -> (B) -> C) -> (A, B) -> C {
    return { f($0)($1) }
}

/// Uncurries a curried ternary function `f`, producing a function which can be applied to a tuple.
public func uncurry<A, B, C, D>(_ f: @escaping (A) -> (B) -> (C) -> D) -> (A, B, C) -> D {
    return { f($0)($1)($2) }
}

/// Uncurries a curried quaternary function `f`, producing a function which can be applied to a tuple.
public func uncurry<A, B, C, D, E>(_ f: @escaping (A) -> (B) -> (C) -> (D) -> E) -> (A, B, C, D) -> E {
    return { f($0)($1)($2)($3) }
}
