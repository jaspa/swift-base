//
//  Swift-Base.h
//  Swift-Base
//
//  Created by Janek Spaderna on 18.04.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for Swift-Base.
FOUNDATION_EXPORT double Swift_BaseVersionNumber;

//! Project version string for Swift-Base.
FOUNDATION_EXPORT const unsigned char Swift_BaseVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Swift_Base/PublicHeader.h>


