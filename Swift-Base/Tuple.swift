//
//  Tuple.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 09.05.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation

/// Return the first element of a 2-element tuple
public func fst<T, U>(_ x: (T, U)) -> T { return x.0 }

/// Return the second element of a 2-element tuple
public func snd<T, U>(_ x: (T, U)) -> U { return x.1 }

/// Swap the two elements of a 2-element tuple
public func swap<T, U>(_ x: (T, U)) -> (U, T) { return (x.1, x.0) }
