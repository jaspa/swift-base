//
//  Composition.swift
//  Swift-Base
//
//  Created by Janek Spaderna on 30.04.15.
//  Copyright (c) 2015 Janek Spaderna. All rights reserved.
//

import Foundation

precedencegroup CompositionPrecedence {
  higherThan: BitwiseShiftPrecedence
  associativity: right
}

infix operator • : CompositionPrecedence

/// Function composition.
///
///   (f • g)(x) == f(g(x))
public
func •<A, B, C>(f: @escaping (B) -> C, g: @escaping (A) -> B) -> (A) -> C {
    return { f(g($0)) }
}

/// Function composition for throwing functions.
///
///   try (f • g)(x) == try f(try g(x))
public
func •<A, B, C>(f: @escaping (B) throws -> C, g: @escaping (A) throws -> B)
  -> (A) throws -> C
{
  return { try f(try g($0)) }
}
